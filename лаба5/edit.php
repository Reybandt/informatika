<html>
<body>

<?php
require_once 'connection.php';

$link = mysqli_connect($host, $user, $password, $database) 
    or die ("Ошибка подключения к базе данных" . mysqli_error());

echo "Connected<br>";

$id_record = $_GET["id"];
$sql = "SELECT alpinist.FIO as FIO, alpinist.adress as adress, raspredelenie.poriadok_raspredelenia as poriadok_raspredelenia, raspredelenie.data_raspredelenia as data_raspredelenia
FROM alpinist, raspredelenie 
WHERE alpinist.id_alpinist = raspredelenie.id_alpinist 
AND alpinist.id_alpinist = " . $id_record . ";";

$result = mysqli_query($link, $sql);
$record = mysqli_fetch_assoc($result);

?>

<form action="update.php" method="get">
    <input type="hidden" name="id_record" value=<?php echo $id_record; ?>><br>
    <p>ФИО:</p>
    <input type="text" name="FIO" value=<?php echo $record['FIO']; ?>><br>
	<p>Адресс:</p>
	<input type="text" name="adress" value=<?php echo $record['adress']; ?>><br>
	<p>Порядок распределения:</p>
	<input type="text" name="poriadok_raspredelenia" value=<?php echo $record['poriadok_raspredelenia']; ?>><br>
    <p>Дата распределения:</p>
    <input type="text" name="data_raspredelenia" value=<?php echo $record['data_raspredelenia']; ?>><br>
    <p><input type="submit"></p>
</form>

</body>
</html>