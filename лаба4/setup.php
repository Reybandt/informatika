<?php
require_once 'connection.php'; // подключаем скрипт

$link = mysqli_connect($host, $user, $password, $database) 
	or die("Ошибка " . mysqli_error($link));


//CREATE TABLES

$sql = "CREATE TABLE alpinistski_club (
	id_alpinistski_club int(10) not null,	
	email varchar(50) not null,
	nazvanie varchar(100) not null,
	kontaktnoe_lizo varchar(100) not null,
	mesto_raspolozhenia varchar(50),
	primary key (id_alpinistski_club)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully";
} else {
	echo "Error creating table: " . mysqli_error($link);
}

$sql = "CREATE TABLE alpinist (
	id_alpinist int(10) not null,
	id_alpinistski_club int(10) not null,
	FIO varchar(50) not null,
	adress varchar(100) not null,
	nomer_telephona int(10) not null,
	primary key (id_alpinist),
	foreign key (id_alpinistski_club) references alpinistski_club (id_alpinistski_club)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully";
} else {
	echo "Error creating table: " . mysqli_error($link);
}

$sql = "CREATE TABLE alpinistskaya_group (
	id_group int(10) not null,	
	kolvo_alpinistov int(50) not null,
	primary key (id_group)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully";
} else {
	echo "Error creating table: " . mysqli_error($link);
}

$sql = "CREATE TABLE raspredelenie (
	id_alpinist int(10) not null,
	id_group int(10) not null,
	poriadok_raspredelenia varchar(50) not null,	
	data_raspredelenia date not null,
	foreign key (id_alpinist) references alpinist (id_alpinist),
	foreign key (id_group) references alpinistskaya_group (id_group)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully";
} else {
	echo "Error creating table: " . mysqli_error($link);
}

$sql = "CREATE TABLE gora (
	nazvanie_gora varchar(50) not null,	
	data_voshozhdenia date not null,
	kolvo_voshozhdeni int(50) not null,
	mesto_raspolozhenia varchar(100) not null,
	height int(20) not null,
	pokorennie_vershini varchar(50) not null,
	nepokorennie_vershini varchar(50) not null,
	primary key (nazvanie_gora)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully";
} else {
	echo "Error creating table: " . mysqli_error($link);
}

$sql = "CREATE TABLE marshrut (
	id_marshruta int(10) not null,
	prodolzhitelnost int(50) not null,
	nazvanie_gora varchar(50) not null,
	primary key (id_marshruta),
	foreign key (nazvanie_gora) references gora (nazvanie_gora)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully";
} else {
	echo "Error creating table: " . mysqli_error($link);
}

$sql = "CREATE TABLE neshtatnie_situations (
	id_travmirovannogo int(10) not null,	
	xaracter_travmi varchar(50) not null,
	vosmoshni_letalnu_ishod varchar(10) not null,
	primary key (id_travmirovannogo)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully";
} else {
	echo "Error creating table: " . mysqli_error($link);
}

$sql = "CREATE TABLE voshozhdenie_na_goru (
	id_marshruta int(10) not null,
	id_group int(10) not null,
	id_travmirovannogo int(10) not null,
	data_nachala date not null,	
	data_koncza date not null,
	foreign key (id_marshruta) references marshrut (id_marshruta),
	foreign key (id_group) references alpinistskaya_group (id_group),
	foreign key (id_travmirovannogo) references neshtatnie_situations (id_travmirovannogo)
	)";
if (mysqli_query($link, $sql)) {
	echo "Table created successfully";
} else {
	echo "Error creating table: " . mysqli_error($link);
}

//INSERT alpinistski_club

$query = "INSERT INTO alpinistski_club VALUES(1, 'alp@mail.ru','Покорители', 'Дмитрий Андреев', 'Россия, Сочи')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO alpinistski_club VALUES(2, '1@mail.ru','Непокоренные', 'Андрей Иванов', 'Россия, СПб')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

//INSERT alpinist

$query = "INSERT INTO alpinist VALUES(12, 1, 'Антон Игоревич','Россия, Тверь', 5478)";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO alpinist VALUES(18, 2, 'Денис Некифоров','Россия, Казань', 5555)";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO alpinist VALUES(21, 1, 'Игорь Антонов','Россия, МСК', 1111)";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO alpinist VALUES(22, 2, 'Осип Осипов','Россия, СПб', 3333)";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO alpinist VALUES(99, 1, 'Дима Димонов','Россия, Краснодар', 7777)";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

//INSERT alpinistskaya_group

$query = "INSERT INTO alpinistskaya_group VALUES(1225, 3)";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO alpinistskaya_group VALUES(5221, 2)";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

//INSERT raspredelenie

$query = "INSERT INTO raspredelenie VALUES(12, 1225, 'Очередь', '2018-01-01')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO raspredelenie VALUES(18, 1225, 'Очередь','2018-01-05')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO raspredelenie VALUES(22, 5221, 'Очередь','2018-01-08')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO raspredelenie VALUES(21, 1225, 'Очередь','2018-01-11')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO raspredelenie VALUES(99, 5221, 'Очередь','2018-01-12')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

//INSERT gora

$query = "INSERT INTO gora VALUES('Аконкагуа', '2018-01-18', 5, 'Аргентина', 6900, 'Аймара', 'Неаймара')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO gora VALUES('Гималаи', '2018-01-23', 5, 'Евразия, Тибет', 8848, 'Эверест', 'Намопраоов')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

//INSERT marshrut

$query = "INSERT INTO marshrut VALUES(2221, 4,'Гималаи')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO marshrut VALUES(3321, 2,'Аконкагуа')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO marshrut VALUES(4004, 6,'Аконкагуа')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO marshrut VALUES(5015, 1,'Гималаи')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO marshrut VALUES(6789, 3,'Гималаи')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

//INSERT neshtatnie_situations

$query = "INSERT INTO neshtatnie_situations VALUES(12, 'Травма ноги', '-')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO neshtatnie_situations VALUES(18, 'Травма позвоночника', '-')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO neshtatnie_situations VALUES(21, 'Травма голени', '-')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO neshtatnie_situations VALUES(22, 'Травма головы', '-')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO neshtatnie_situations VALUES(99, 'Травма руки', '-')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

//INSERT voshozhdenie_na_goru

$query = "INSERT INTO voshozhdenie_na_goru VALUES(5015, 1225, 22, '2018-01-10', '2018-02-06')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO voshozhdenie_na_goru VALUES(2221, 1225, 21, '2017-01-11', '2018-02-03')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO voshozhdenie_na_goru VALUES(3321, 5221, 12, '2018-01-15', '2018-02-15')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO voshozhdenie_na_goru VALUES(4004, 5221, 18, '2017-01-14', '2018-02-01')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

$query = "INSERT INTO voshozhdenie_na_goru VALUES(6789, 5221, 99, '2016-01-12', '2018-02-20')";
if (mysqli_query($link, $query)) {
	echo "Данные добавлены<br>";
} else {
	echo "Error insert data: " . mysqli_error($link) . "<br>";
}

mysqli_close($link);
?>